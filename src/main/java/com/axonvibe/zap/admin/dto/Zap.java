package com.axonvibe.zap.admin.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Created by vnqthanh on 01/12/2015.
 */
@Document(collection = "Zaps")
public class Zap {

    @Id
    private String _id;

    @Field("id")
    private String zapId;

    private String type;

    private String vid;

    @Field("lat")
    private double latitude;

    @Field("lon")
    private double longitude;

    private Location loc;

    private List<Report> reports;
    private Long blocked;
    private Long moderated;
    private Long time;
    private Long expiry;
    private Long deleted;
    private String name;
    private String image;
    private String message;
    private String photo;
    private double commentCount;
    private double likeCount;
    private List<Like> likes;
    private List<Comment> comments;

    public Long getModerated() {
        return moderated;
    }

    public void setModerated(Long moderated) {
        this.moderated = moderated;
    }

    public Long getBlocked() {
        return blocked;
    }

    public void setBlocked(Long blocked) {
        this.blocked = blocked;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getExpiry() {
        return expiry;
    }

    public void setExpiry(Long expiry) {
        this.expiry = expiry;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public double getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(double commentCount) {
        this.commentCount = commentCount;
    }

    public double getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(double likeCount) {
        this.likeCount = likeCount;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getZapId() {
        return zapId;
    }

    public void setZapId(String zapId) {
        this.zapId = zapId;
    }

}
