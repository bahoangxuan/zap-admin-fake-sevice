package com.axonvibe.zap.admin.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by hxba on 1/4/2016.
 */
@Document(collection = "ZapDevices")
public class ZapDevices {
    @Id
    private String _id;
    private String vid;
    private String userId;
    private String appBuild;
    private String appVersion;
    private boolean collectLocations;
    private String displayName;
    private String language;
    private boolean newPushToken;
    private String platform;
    private String sdk_build;
    private String sdk_version;
    private String timezone;
    private String userAgent;
    private Date datetime_registered;
    private String facebookProfileImageURL;
    private String clientId;
    private Visit location;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppBuild() {
        return appBuild;
    }

    public void setAppBuild(String appBuild) {
        this.appBuild = appBuild;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public boolean isCollectLocations() {
        return collectLocations;
    }

    public void setCollectLocations(boolean collectLocations) {
        this.collectLocations = collectLocations;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isNewPushToken() {
        return newPushToken;
    }

    public void setNewPushToken(boolean newPushToken) {
        this.newPushToken = newPushToken;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSdk_build() {
        return sdk_build;
    }

    public void setSdk_build(String sdk_build) {
        this.sdk_build = sdk_build;
    }

    public String getSdk_version() {
        return sdk_version;
    }

    public void setSdk_version(String sdk_version) {
        this.sdk_version = sdk_version;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Date getDatetime_registered() {
        return datetime_registered;
    }

    public void setDatetime_registered(Date datetime_registered) {
        this.datetime_registered = datetime_registered;
    }

    public String getFacebookProfileImageURL() {
        return facebookProfileImageURL;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Visit getLocation() {
        return location;
    }

    public void setLocation(Visit location) {
        this.location = location;
    }

    public void setFacebookProfileImageURL(String facebookProfileImageURL) {
        this.facebookProfileImageURL = facebookProfileImageURL;
    }

}
