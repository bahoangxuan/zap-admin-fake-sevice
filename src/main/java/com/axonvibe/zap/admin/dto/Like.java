package com.axonvibe.zap.admin.dto;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by vnqthanh on 09/12/2015.
 */
public class Like {

    private String type;

    private Long time;

    private String vid;

    @Field("id")
    private String likeId;

    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getLikeId() {
        return likeId;
    }

    public void setLikeId(String likeId) {
        this.likeId = likeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
