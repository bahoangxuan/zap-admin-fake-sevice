package com.axonvibe.zap.admin.dto;

/**
 * Created by hxba on 1/13/2016.
 */
public class Visit {
    private Location WORK;
    private Location CURRENT;
    private Location HOME;

    public Location getWORK() {
        return WORK;
    }

    public void setWORK(Location WORK) {
        this.WORK = WORK;
    }

    public Location getHOME() {
        return HOME;
    }

    public void setHOME(Location HOME) {
        this.HOME = HOME;
    }

    public Location getCURRENT() {
        return CURRENT;
    }

    public void setCURRENT(Location CURRENT) {
        this.CURRENT = CURRENT;
    }
}
