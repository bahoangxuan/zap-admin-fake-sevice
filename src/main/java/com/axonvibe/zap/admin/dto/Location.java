package com.axonvibe.zap.admin.dto;

import java.util.Date;

/**
 * Created by vnqthanh on 09/12/2015.
 */
public class Location {

    private String type;

    private Date datetime_recorded;

    private double[] coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public Date getDatetime_recorded() {
        return datetime_recorded;
    }

    public void setDatetime_recorded(Date datetime_recorded) {
        this.datetime_recorded = datetime_recorded;
    }
}
