package com.axonvibe.zap.admin.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by hxba on 1/12/2016.
 */
@Document(collection = "CaptureClient")
public class CaptureClient {
    @Id
    private String _id;
    private String vid;
    private String jwt;
    private String clientId;
    private long datetime_registered;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public long getDatetime_registered() {
        return datetime_registered;
    }

    public void setDatetime_registered(long datetime_registered) {
        this.datetime_registered = datetime_registered;
    }
}
