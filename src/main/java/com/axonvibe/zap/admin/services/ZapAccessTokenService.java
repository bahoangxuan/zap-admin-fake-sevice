package com.axonvibe.zap.admin.services;

import com.axonvibe.zap.admin.controllers.ApiController;
import com.axonvibe.zap.admin.dto.*;
import com.axonvibe.zap.admin.repositories.CaptureClientRepository;
import com.axonvibe.zap.admin.repositories.ContentClientRepository;
import com.axonvibe.zap.admin.repositories.ZapAccessTokensRepository;
import com.axonvibe.zap.admin.repositories.ZapDevicesRepository;
import com.axonvibe.zap.admin.repositories2.ZapsRepository;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by hxba on 12/31/2015.
 */
@Service
public class ZapAccessTokenService {
    @Autowired
    ZapAccessTokensRepository zapAccessTokensRepository;

    @Autowired
    ZapsRepository zapsRepository;

    @Autowired
    ZapDevicesRepository zapDevicesRepository;

    @Autowired
    ContentClientRepository contentClientRepository;

    @Autowired
    CaptureClientRepository captureClientRepository;

    public String getAccessToken(String jwt) {
        List<ZapAccessTokens> accessTokensList = zapAccessTokensRepository.findByJwt(jwt);
        if (accessTokensList != null && accessTokensList.size() > 0) {
            return accessTokensList.get(0).getToken();
        }
        return "";
    }

    public boolean checkTokenValid(String token) {
        return zapAccessTokensRepository.findByToken(token).size() > 0;
    }

    public String getVidByToken(String token) {
        String vid = "";
        List<ZapAccessTokens> accessTokenses = zapAccessTokensRepository.findByToken(token);
        if (accessTokenses.size() > 0) {
            String jwtContent = accessTokenses.get(0).getJwt();
            List<ContentClient> contentClients = contentClientRepository.findByJwt(jwtContent);
            if (contentClients.size() > 0) {
                List<ZapDevices> devices = zapDevicesRepository.findByUserId(contentClients.get(0).getUserId());
                if (devices.size() > 0)
                    return devices.get(0).getVid();
            }
        }
        return vid;
    }

    public org.json.simple.JSONObject saveZap(org.json.JSONObject dbObject, String id, String vid) {
        Zap zap = new Zap();
        zap.setVid(vid);
        if (!dbObject.isNull("image")) {
            zap.setImage((String) dbObject.get("image"));
        } else {
            zap.setImage("");
        }
        zap.setType((String) dbObject.get("type"));
        if (dbObject.has("photo")) {
            zap.setPhoto((String) dbObject.get("photo"));
        }
        if (Strings.isNullOrEmpty(id))
            zap.setZapId(UUID.randomUUID().toString());
        else
            zap.setZapId((String) dbObject.get("id"));
        zap.setTime((Long) dbObject.get("time"));
        zap.setMessage((String) dbObject.get("message"));
        zap.setName((String) dbObject.get("name"));
        zap.setLatitude((Double) dbObject.get("lat"));
        zap.setLongitude((Double) dbObject.get("lon"));
        zap.setExpiry(new DateTime().plusDays(7).getMillis());
        Location loc = new Location();
        loc.setType("Point");
        loc.setCoordinates(new double[]{zap.getLongitude(), zap.getLatitude()});
        zap.setLoc(loc);
        zapsRepository.save(zap);
        org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
        obj.put("id", zap.getZapId());
        return obj;
    }

    public org.json.simple.JSONObject likeZap(JSONObject obj, String zapId, String deviceId) {
        Zap zap = zapsRepository.findByZapId(zapId);
        double likeCount = 0;
        Like like = new Like();
        if (zap != null) {
            likeCount = zap.getLikeCount();
            long likeTime = (long) obj.get("time");
            List<Like> likes = zap.getLikes();
            like.setLikeId((String) obj.get("id"));
            if (likes == null) {
                likes = new ArrayList<>();
            }
            like.setType((String) obj.get("type"));
            like.setTime(likeTime);
            like.setName((String) obj.get("name"));
            like.setVid(deviceId);
            likes.add(like);
            likeCount++;
            zap.setLikes(likes);
            zap.setExpiry(new DateTime(likeTime).plusDays(7).getMillis());
            zap.setLikeCount(likeCount);
            zapsRepository.save(zap);
        }
        org.json.simple.JSONObject objRet = new org.json.simple.JSONObject();
        objRet.put("id", like.getLikeId());
        objRet.put("vid", like.getVid());
        objRet.put("name", like.getName());
        objRet.put("time", like.getTime());
        objRet.put("type", like.getType());
        return objRet;
    }

    public List<Like> getLikes(String zapId) {
        Zap zap = zapsRepository.findByZapId(zapId);
        List<Like> likes = new ArrayList<>();
        if (zap != null) {
            likes = zap.getLikes();
        }
        return likes;
    }

    public String delete(String zapId) {
        Zap zap = zapsRepository.findByZapId(zapId);
        if (zap != null) {
            zap.setDeleted(new Date().getTime());
            zapsRepository.save(zap);
        }
        return "{}";

    }

    public Object getComments(String zapId) {
        Zap zap = zapsRepository.findByZapId(zapId);
        List<Comment> comments = new ArrayList<>();
        if (zap != null) {
            comments = zap.getComments();
        }
        return comments;
    }

    public Object commentZap(JSONObject obj, String zapId, String device) {
        Zap zap = zapsRepository.findByZapId(zapId);
        double likeCount = 0;
        if (zap != null) {
            likeCount = zap.getCommentCount();
            List<Comment> comments = zap.getComments();
            long commentTime = (long) obj.get("time");
            Comment like = new Comment();
            if (comments == null) {
                comments = new ArrayList<>();
            }
            like.setCommentId(UUID.randomUUID().toString());
            like.setType((String) obj.get("type"));
            like.setTime(commentTime);
            like.setName((String) obj.get("name"));
            like.setMessage((String) obj.get("message"));
            like.setVid(device);
            comments.add(like);
            likeCount++;
            zap.setComments(comments);
            zap.setCommentCount(likeCount);
            zap.setExpiry(new DateTime(commentTime).plusDays(7).getMillis());
            zapsRepository.save(zap);
        }

        return new org.json.simple.JSONObject();
    }

    public org.json.simple.JSONObject registerContentDevice(ContentClient zapDevices) {
        contentClientRepository.save(zapDevices);
        org.json.simple.JSONObject retObj = new org.json.simple.JSONObject();
        retObj.put("jwt", zapDevices.getJwt());
        ZapAccessTokens zapAccessTokens = new ZapAccessTokens();
        zapAccessTokens.setJwt(zapDevices.getJwt());
        zapAccessTokens.setToken(DigestUtils.sha1Hex(zapDevices.getJwt()));
        zapAccessTokens.setClientId(zapDevices.getClientId());
        zapAccessTokensRepository.save(zapAccessTokens);
        return retObj;
    }

    public org.json.simple.JSONObject registerCaptureDevice(CaptureClient zapDevices) {
        captureClientRepository.save(zapDevices);
        org.json.simple.JSONObject retObj = new org.json.simple.JSONObject();
        retObj.put("jwt", zapDevices.getJwt());
        ZapAccessTokens zapAccessTokens = new ZapAccessTokens();
        zapAccessTokens.setJwt(zapDevices.getJwt());
        zapAccessTokens.setToken(DigestUtils.sha1Hex(zapDevices.getJwt()));
        zapAccessTokens.setClientId(zapDevices.getClientId());
        zapAccessTokensRepository.save(zapAccessTokens);
        return retObj;
    }


    public List<ZapDevices> getDevices(String field, int maxRecord) {
        Pageable pageable = new PageRequest(0, maxRecord, Sort.Direction.DESC, field);
        Page<ZapDevices> page = zapDevicesRepository.findAll(pageable);
        List<ZapDevices> devices = Lists.newArrayList(page);
        for (ZapDevices device : devices) {

        }
        return devices;
    }

    public ZapDevices saveDevice(JSONObject obj) {
        ZapDevices zapDevices = new ZapDevices();
        zapDevices.setVid((String) obj.get("vid"));
        zapDevices.setUserId((String) obj.get("userId"));
        zapDevices.setUserAgent(ApiController.AXON_VIBE_AA_DEV_ADMIN_TOOL);
        zapDevices.setTimezone((String) obj.get("timezone"));
        zapDevices.setAppVersion((String) obj.get("appVersion"));
        zapDevices.setClientId((String) obj.get("clientId"));
        zapDevices.setCollectLocations((Boolean) obj.get("collectLocations"));
        zapDevices.setDisplayName((String) obj.get("displayName"));
        zapDevices.setLanguage((String) obj.get("language"));
        zapDevices.setNewPushToken((Boolean) obj.get("newPushToken"));
        zapDevices.setPlatform((String) obj.get("platform"));
        zapDevices.setSdk_build((String) obj.get("sdk_build"));
        zapDevices.setSdk_version((String) obj.get("sdk_version"));
        zapDevices.setDatetime_registered(new DateTime(DateTimeZone.UTC).toDate());
        zapDevicesRepository.save(zapDevices);
        return zapDevices;
    }
}
