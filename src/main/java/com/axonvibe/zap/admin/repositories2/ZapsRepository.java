package com.axonvibe.zap.admin.repositories2;

import com.axonvibe.zap.admin.dto.Zap;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by hxba on 12/31/2015.
 */
public interface ZapsRepository extends MongoRepository<Zap, String> {
    Zap findByZapId(final String zapId);

}
