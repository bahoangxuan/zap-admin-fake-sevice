package com.axonvibe.zap.admin.repositories2;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * Created by hxba on 12/23/2015.
 */
public interface ZapRepositoryCustom {
    void saveZap(Query query, Update obj);
}
