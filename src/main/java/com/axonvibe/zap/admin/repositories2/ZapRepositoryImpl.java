package com.axonvibe.zap.admin.repositories2;

import com.axonvibe.zap.admin.dto.Zap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * Created by hxba on 12/23/2015.
 */
public class ZapRepositoryImpl implements ZapRepositoryCustom {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveZap(Query query,Update obj) {
        mongoTemplate.updateFirst(query, obj, Zap.class);
    }
}
