package com.axonvibe.zap.admin.repositories;

import com.axonvibe.zap.admin.dto.CaptureClient;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by hxba on 1/12/2016.
 */
public interface CaptureClientRepository extends MongoRepository<CaptureClient, String> {
}
