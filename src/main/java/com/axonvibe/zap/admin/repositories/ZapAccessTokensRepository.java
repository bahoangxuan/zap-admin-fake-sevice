package com.axonvibe.zap.admin.repositories;

import com.axonvibe.zap.admin.dto.ZapAccessTokens;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by hxba on 12/31/2015.
 */
public interface ZapAccessTokensRepository extends MongoRepository<ZapAccessTokens, String> {
    List<ZapAccessTokens> findByJwt(final String jwt);
    List<ZapAccessTokens> findByToken(final String token);
}
