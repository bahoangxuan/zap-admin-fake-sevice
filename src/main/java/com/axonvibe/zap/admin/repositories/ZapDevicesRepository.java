package com.axonvibe.zap.admin.repositories;

import com.axonvibe.zap.admin.dto.ZapDevices;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by hxba on 1/4/2016.
 */
public interface ZapDevicesRepository extends PagingAndSortingRepository<ZapDevices, String> {
    List<ZapDevices> findByUserId(final String userId);
}
