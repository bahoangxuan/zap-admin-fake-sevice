package com.axonvibe.zap.admin.repositories;

import com.axonvibe.zap.admin.dto.ContentClient;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by hxba on 1/12/2016.
 */
public interface ContentClientRepository extends MongoRepository<ContentClient, String> {
    List<ContentClient> findByJwt(final String jwt);
}
