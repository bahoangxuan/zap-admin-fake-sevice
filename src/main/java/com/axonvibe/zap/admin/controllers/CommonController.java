package com.axonvibe.zap.admin.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hxviet on 10/20/2015.
 */
@RestController
@RequestMapping("/")
public class CommonController {
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody String home() {
        return "OK";
    }
}
