package com.axonvibe.zap.admin.controllers;

import com.axonvibe.zap.admin.dto.CaptureClient;
import com.axonvibe.zap.admin.dto.ContentClient;
import com.axonvibe.zap.admin.services.ZapAccessTokenService;
import com.google.common.base.Strings;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Created by hxviet on 10/20/2015.
 */

@RestController
@RequestMapping("/v1")
public class ApiController {

    public static final String AXON_VIBE_AA_DEV_ADMIN_TOOL = "Axon Vibe AA-DEV Admin Tool";
    @Autowired
    ZapAccessTokenService zapAccessTokenService;

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);


    @RequestMapping(value = "/oauth/register", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject register(@RequestHeader(value = "X-Vibe-Device-ID") String vid,
                        @RequestHeader(value = "Content-Type") String type,
                        @RequestHeader(value = "X-Vibe-Client-ID") String clientId) {
        JSONObject jObject = null;
        if ("av_vibe_app".equals(clientId)) {
            String needToHex = vid + new Date().getTime();
            String hex = DigestUtils.sha1Hex(needToHex);
            ContentClient dev = new ContentClient();
            dev.setUserId(vid);
            dev.setClientId(clientId);
            dev.setJwt(hex);
            dev.setDatetime_registered(new Date().getTime());
            jObject = zapAccessTokenService.registerContentDevice(dev);
        }
        else if ("av_sdk".equals(clientId)) {
            String needToHex = vid + new Date().getTime();
            String hex = DigestUtils.sha1Hex(needToHex);
            CaptureClient dev = new CaptureClient();
            dev.setVid(vid);
            dev.setClientId(clientId);
            dev.setJwt(hex);
            dev.setDatetime_registered(new Date().getTime());
            jObject = zapAccessTokenService.registerCaptureDevice(dev);
        }
        return jObject;
    }

    @RequestMapping(value = "/device", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject registerDevice(@RequestHeader(value = "User-Agent") String agent,
                              @RequestHeader(value = "Authorization") String authen,
                              @RequestBody String zapBody,
                              HttpServletResponse response) {
        JSONObject jObject = null;
        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted && AXON_VIBE_AA_DEV_ADMIN_TOOL.equals(agent)) {
                org.json.JSONObject obj = new org.json.JSONObject(zapBody);
                returnObj.put("data", zapAccessTokenService.saveDevice(obj));
                returnObj.put("success", true);
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        return jObject;
    }

    @RequestMapping(value = "/oauth/token", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject getToken(@RequestParam(value = "grant_type") String param,
                        @RequestHeader(value = "X-Vibe-JWT") String header1,
                        @RequestHeader(value = "Content-Type") String header2) {
        String token = "";
        JSONObject obj = new JSONObject();
        if ("client_credentials".equals(param)) {
            token = zapAccessTokenService.getAccessToken(header1);
        }
        obj.put("access_token", token);
        obj.put("token_type", "bearer");
        obj.put("expires_in", 599);
        obj.put("scope", "read");
        return obj;
    }

    @RequestMapping(value = "/cloudinary/credentials", method = RequestMethod.GET)
    public
    @ResponseBody
    JSONObject getCloudinaryCredentials(@RequestHeader(value = "Authorization") String authen,
                                        @RequestHeader(value = "Content-Type") String contentType) {
        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                returnObj.put("data", genCloudinaryData());
                returnObj.put("success", true);
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
            }
        }
        return returnObj;
    }

    @RequestMapping(value = {"/zap/{zapId}", "/zap"}, method = RequestMethod.PUT)
    public
    @ResponseBody
    JSONObject putZap(@PathVariable Map<String, String> pathVariables,
                      @RequestBody String zapBody,
                      @RequestHeader(value = "Authorization") String authen,
                      @RequestHeader(value = "Content-type") String contentType,
                      HttpServletResponse response) {

        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            String zapId = pathVariables.get("zapId");
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                String vid = zapAccessTokenService.getVidByToken(token);
                if (!Strings.isNullOrEmpty(vid)) {
                    org.json.JSONObject obj = new org.json.JSONObject(zapBody);
                    returnObj.put("data", zapAccessTokenService.saveZap(obj, zapId, vid));
                    returnObj.put("success", true);
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        return returnObj;
    }

    @RequestMapping(value = "/zap/{zapId}/like", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject zapLike(@PathVariable String zapId,
                       @RequestBody String likeBody,
                       @RequestHeader(value = "Authorization") String authen,
                       @RequestHeader(value = "Content-type") String contentType,
                       HttpServletResponse response) {

        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                String vid = zapAccessTokenService.getVidByToken(token);
                if (!Strings.isNullOrEmpty(vid)) {
                    org.json.JSONObject obj = new org.json.JSONObject(likeBody);
                    returnObj.put("data", zapAccessTokenService.likeZap(obj, zapId, vid));
                    returnObj.put("success", true);
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        return returnObj;
    }

    @RequestMapping(value = "/zap/{zapId}/likes", method = RequestMethod.GET)
    public
    @ResponseBody
    JSONObject getLikes(@PathVariable String zapId,
                        @RequestHeader(value = "Authorization") String authen,
                        @RequestHeader(value = "Content-type") String contentType) {

        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                returnObj.put("data", zapAccessTokenService.getLikes(zapId));
                returnObj.put("success", true);
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
            }
        }
        return returnObj;
    }

    @RequestMapping(value = "/zap/{zapId}/comments", method = RequestMethod.GET)
    public
    @ResponseBody
    JSONObject getComments(@PathVariable String zapId,
                           @RequestHeader(value = "Authorization") String authen,
                           @RequestHeader(value = "Content-type") String contentType) {

        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                returnObj.put("data", zapAccessTokenService.getComments(zapId));
                returnObj.put("success", true);
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
            }
        }
        return returnObj;
    }

    @RequestMapping(value = "/zap/{zapId}/comment", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject zapComment(@PathVariable String zapId,
                          @RequestBody String commentBody,
                          @RequestHeader(value = "Authorization") String authen,
                          @RequestHeader(value = "Content-type") String contentType,
                          HttpServletResponse response) {

        logger.debug("add comment {0}", zapId);
        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                String vid = zapAccessTokenService.getVidByToken(token);
                if (!Strings.isNullOrEmpty(vid)) {
                    org.json.JSONObject obj = new org.json.JSONObject(commentBody);
                    returnObj.put("data", zapAccessTokenService.commentZap(obj, zapId, vid));
                    returnObj.put("success", true);
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        return returnObj;
    }

    @RequestMapping(value = "/zap/{zapId}", method = RequestMethod.DELETE)
    public
    @ResponseBody
    JSONObject delete(@PathVariable String zapId,
                      @RequestHeader(value = "Authorization") String authen) {

        String[] arr = authen.split(" ");
        JSONObject returnObj = new JSONObject();
        if (arr.length == 2) {
            String token = arr[1];
            boolean isExisted = zapAccessTokenService.checkTokenValid(token);
            if (isExisted) {
                returnObj.put("data", zapAccessTokenService.delete(zapId));
                returnObj.put("success", true);
            } else {
                returnObj.put("data", "{}");
                returnObj.put("success", false);
            }
        }
        return returnObj;
    }

    @RequestMapping(value = "/device", method = RequestMethod.GET)
    public
    @ResponseBody
    JSONObject getDevices(@RequestParam(value = "sort", defaultValue = "datetime_registered") String sortField,
                          @RequestParam(value = "limit", defaultValue = "50") int limit) {

        JSONObject returnObj = new JSONObject();
        returnObj.put("data", zapAccessTokenService.getDevices(sortField, limit));
        returnObj.put("success", true);
        return returnObj;
    }


    private JSONObject genCloudinaryData() {
        String apiKey = "945354845131369";
        String apiSecret = "mT4xJSzCyGVNnmi-mSUEb2DugTE";
        long timeStamp = new Date().getTime();
        String pId = UUID.randomUUID().toString();
        String needToHex = "public_id=" + pId + "&timestamp=" + timeStamp + apiSecret;
        String signature = DigestUtils.sha1Hex(needToHex);
        JSONObject obj = new JSONObject();
        obj.put("apiKey", apiKey);
        obj.put("publicId", pId);
        obj.put("timestamp", timeStamp);
        obj.put("signature", signature);
        return obj;
    }

}
